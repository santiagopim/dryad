#+TITLE: Dryad
#+SUBTITLE: View and edit programs as trees
#+DESCRIPTION: User Manual
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="style.css" />

#+BEGIN_EXPORT html
<a href="https://liberapay.com/contrapunctus/donate">
  <img alt="Donate using Liberapay" src="https://img.shields.io/liberapay/receives/contrapunctus.svg?logo=liberapay">
</a>
#+END_EXPORT

* Explanation
:PROPERTIES:
:CUSTOM_ID: explanation
:END:
Dryad is a code browser in CLIM which aims to provide a better way of browsing and editing code, without requiring any changes to existing codebases. It stemmed from the author's desire to enjoy the benefits of structuring source code as a single-file Org literate program -
1. The ability to visualize the program as a tree, at different levels of granularity (as provided by =org-cycle=, =org-global-cycle=, and =org-kill-note-or-show-branches=)
   + This is also useful in situations where you have to jump back and forth between two or more definitions or components.
2. The ability to filter the tree based on names, relations, and metadata (=org-sparse-tree=)
3. A uniform interface to jump to any node in the tree, without having to break flow by thinking about the project/tree structure (=imenus=)

...without all the issues associated with Org literate programs -
1. The majority of projects are not single-file Org literate programs. Even if they were, different authors would use Org in different ways to organize their programs, removing the possibility of some or all of the aforementioned means of visualization/navigation.
2. Converting a conventional project to an Org literate program is considerable effort. Additionally, maintaining code metadata as Org elements (e.g. definition names in headings, and definition types in tags or properties) requires error-prone manual synchronization between the two.
3. Most tools (linters etc) do not support literate programs.
4. Emacs cannot soft wrap different pieces of text differently within the same buffer. Thus, you must choose between -
   1. unwrapped text (good for code, bad for natural language),
   2. all text soft-wrapped to a column (good for natural language, bad for code),
   3. all text soft-wrapped to the window width (bad for natural language if the window is wide, bad for code if the window is narrow), or
   4. hard-wrapped text (only suitable for display on specific devices).
5. Org/Emacs is slow at syntax highlighting large files. While it's /hypothetically/ possible to implement Org performantly in other environments, in practice, this is hindered by Org being complex, ill-specified, and difficult to reimplement exhaustively.
6. Some programmers find it harder to navigate a single large Org literate program than multiple source files and directories.

Dryad currently only supports viewing Common Lisp projects as a tree, but it has been designed to support projects in any language, and even multilingual projects. The author hopes to make Dryad suitable for all stages of any project's lifecycle, from prototype to maturity.


** Drawbacks of Dryad compared to Org literate programs
*** Language support
Adding support for languages (especially non-Lisp languages, since they have a more complex parsing grammar) is likely to involve significant effort. Each language requires a lenient parser - libraries which provide definition introspection alone are not enough, if source files can contain code that is not a definition.

*** Directories cannot contain comments
In Org literate programs, headings are used to define the parts/concerns of a program. In the conventional approach (which Dryad builds on), directories and files are used instead. A source file or piece of code can have comments describing what the file/code is for, but a directory cannot - at least, not in a machine-readable way. This limitation does not exist in Org literate programs.

*** No standard way to structure code within files
In conventionally-structured projects, there is no standard way to structure code within files. There are comment conventions, which may or may not be consistently adhered to. Dryad can at best try to support different conventions, which increases both implementation complexity and the user effort/configuration required.

*** No rich text support
Source comments are plain text and cannot contain formatting information, or embedded static content such as tables or images, let alone dynamic/interactive content. There is no standard markup for them either. At best, Dryad can attempt to support different markup formats, and have the user inform Dryad about which markup is in use.

** License
:PROPERTIES:
:CUSTOM_ID: license
:END:
I'd /like/ for all software to be liberated - transparent, trustable, and accessible for anyone to use, study, or improve.

I'd /like/ anyone using my software to credit me for the work.

I'd /like/ to receive financial support for my efforts, so I can spend all my time doing what I find meaningful.

But I don't want to make demands or threats (e.g. via legal conditions) to accomplish all that, nor restrict my services to only those who can pay.

Thus, =dryad= is released under your choice of [[https://unlicense.org/][Unlicense]] or the [[http://www.wtfpl.net/][WTFPL]].

(See files [[file:UNLICENSE][UNLICENSE]] and [[file:WTFPL][WTFPL]]).
