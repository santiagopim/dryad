
* Prototype
:PROPERTIES:
:CUSTOM_ID: prototype
:END:
** STARTED Incremental redisplay                              :optimization:
:PROPERTIES:
:CUSTOM_ID: incremental-redisplay
:END:
1. [ ] Use =updating-output= to improve folding performance with larger codebases (e.g. Eclector)

** DONE Indent according to tree structure                         :feature:
:PROPERTIES:
:CREATED:  2022-06-06T20:23:58+0530
:CUSTOM_ID: indent-according-to-tree-structure
:END:
** Display beginning of buffer on launch                                :ux:
:PROPERTIES:
:CREATED:  2022-06-06T13:36:05+0530
:CUSTOM_ID: display-beginning-buffer-on-launch
:END:

** DONE Display directories                                        :feature:
:PROPERTIES:
:CREATED:  2022-06-06T14:25:00+0530
:CUSTOM_ID: display-directories
:END:

** DONE Display code blocks as tree nodes                          :feature:
:PROPERTIES:
:CREATED:  2022-06-06T14:27:53+0530
:CUSTOM_ID: display-code-blocks-as-tree-nodes
:END:

** DONE Tree folding commands                                      :feature:
:PROPERTIES:
:CUSTOM_ID: tree-folding-commands
:END:
1. fold tree
2. fold heading (subtree)
3. fold definition

** DONE Improve code block names                                   :feature:
:PROPERTIES:
:CREATED:  2022-06-11T18:57:42+0530
:CUSTOM_ID: improve-code-block-names
:END:

** Display source comments present between code blocks             :feature:
:PROPERTIES:
:CUSTOM_ID: display-source-comments-present-between-code-blocks
:END:

** Improve intra-file tree structure                               :feature:
:PROPERTIES:
:CREATED:  2022-06-06T21:00:35+0530
:CUSTOM_ID: use-source-comments-to-determine-tree-structure
:END:
Files are currently displayed as a flat list of code blocks.

*** Using comments
:PROPERTIES:
:CREATED:  2022-06-18T20:17:01+0530
:CUSTOM_ID: using-comments
:END:
Use structures implied by comments. Study existing codebases to find comment conventions.
+ https://dept-info.labri.fr/~strandh/Teaching/PFS/Common/Strandh-Tutorial/indentation.html
  * Make it possible to use different conventions per project.

The sanest way might be something like Emacs' =outline-mode= - the user tells us what the headings look like (=outline-heading-alist=).

*** Using code patterns
:PROPERTIES:
:CREATED:  2022-06-18T20:16:43+0530
:CUSTOM_ID: using-code-patterns
:END:
A per-language list of "tree modifier" functions could be used to create sections based on different code patterns. Some patterns I've noticed -
1. Multiple consecutive method definitions for the same generic function (optionally preceded by the generic function definition) could be grouped into one section.
   * What happens when there's a helper function for one of these methods in between? If the helper is only used for that method, my preference is to keep it next to it...
2. =(in-package :cl)= + =(defpackage :foo ...)= + =(in-package :foo)=
3. class definition followed by =print-object= and/or =initialize-instance=

** Click to edit code block in editor pane                         :feature:
:PROPERTIES:
:CREATED:  2022-06-07T15:35:36+0530
:CUSTOM_ID: edit-code-blocks
:END:

** User reprogrammability                                          :feature:
:PROPERTIES:
:CREATED:  2022-06-09T09:19:37+0530
:CUSTOM_ID: user-reprogrammability
:END:
1. load a ~/.config/dryad/dryad.lisp (backend configuration)
   * Settings - default view
2. load a ~/.config/dryad/clim.lisp (CLIM GUI configuration)
   * Settings - heading-size-by-depth, heading-colors, code-block-text-style
3. provide one or more functions to change settings, rather than direct modification of global variables or methods/slots.
   + gilberth suggested a function which accepts keyword arguments corresponding to slots. Ideal point to check for invalid user input.

** DONE Define language support/backend protocol                   :feature:
:PROPERTIES:
:CREATED:  2022-06-11T12:35:37+0530
:CUSTOM_ID: define-language-support-backend-protocol
:END:

** More cycling commands                                           :feature:
:PROPERTIES:
:CREATED:  2022-06-17T17:08:06+0530
:CUSTOM_ID: more-cycling-commands
:END:
1. =org-global-cycle=
2. =org-kill-note-or-show-branches=

** Filter tree command                                             :feature:
:PROPERTIES:
:CREATED:  2022-06-17T17:08:49+0530
:CUSTOM_ID: filter-tree-command
:END:
Analogous to =org-sparse-tree=. Filter currently selected tree (or all trees), with live preview.

Queries can contain -
1. "<name>" - search for nodes whose names contain <name>
2. "[<container>/]+<name>" - search for nodes in <container> whose names contain <name>, where <container> is (part of) the name of a file, directory, system definition, or other container.
3. type of definition (function, variable, class, generic, method, struct, package definition, etc)
4. function input type
5. function return type

** Jump-to-node command                                            :feature:
:PROPERTIES:
:CREATED:  2022-06-17T17:15:21+0530
:CUSTOM_ID: jump-to-node-command
:END:

** Insertion commands                                              :feature:
:PROPERTIES:
:CREATED:  2022-06-17T17:09:58+0530
:CUSTOM_ID: insertion-commands
:END:
To go beyond just viewing and editing existing data - commands to "insert" directories, files, source blocks, and text blocks.

** Keyboard-driven navigation and editing                          :feature:
:PROPERTIES:
:CREATED:  2022-06-18T14:02:54+0530
:CUSTOM_ID: cursor-protocol-keyboard-driven-navigation
:END:
Implement a cursor protocol as a temporary measure until we implement [[#WYSIWYG-editing][WYSIWYG editing]]. The cursor operates on containers.

Operations - next/previous visible, next/previous sibling, ascend/descend.

Tree structure modification commands - drag up/down, promote/demote.

** DONE Multiple trees                                             :feature:
:PROPERTIES:
:CREATED:  2022-06-22T22:18:40+0530
:CUSTOM_ID: multiple-trees
:END:
1. [X] Support displaying empty state
2. [X] Command to insert a new tree, given a file/directory
3. [X] Command to remove a tree

** STARTED Views [50%]                                             :feature:
:PROPERTIES:
:CREATED:  2022-06-22T22:17:05+0530
:CUSTOM_ID: views
:END:
Different ways of displaying the contents of the same file/directory.
1. [X] Default view (currently-implemented behaviour)
2. [X] List of views available for a node
3. [ ] Drop-down view selector when inserting new tree or when updating the view of a node
4. [ ] Define presentation translator to present views used in existing nodes

*** DONE ASDF view                                                :feature:
:PROPERTIES:
:CUSTOM_ID: asdf-support
:END:
For a directory, list the systems defined in it; for a system definition file, display the component files and directories as its children, in [[#sort-by-topology-dependency][topological order]].

* Refinements
:PROPERTIES:
:CUSTOM_ID: refinements
:END:
** DONE Move Lisp support into its own file                        :cleanup:
:PROPERTIES:
:CUSTOM_ID: move-lisp-support-into-its-own-file
:END:

** DONE Control displayed files (hidden, version control, etc)     :feature:
:PROPERTIES:
:CREATED:  2022-06-11T12:33:22+0530
:CUSTOM_ID: control-displayed-files-hidden,-version-control,-etc
:END:

** Watch for files changed outside                                 :feature:
:PROPERTIES:
:CREATED:  2022-06-12T08:52:11+0530
:CUSTOM_ID: watch-files-changed-outside
:END:

** Improve UI aesthetics [40%]                                          :ui:
:PROPERTIES:
:CREATED:  2022-06-17T16:44:36+0530
:CUSTOM_ID: improve-ui-aesthetics
:END:
1. [ ] increase heading font size
2. [X] colorize headings
3. [X] use fixed width font for code blocks
4. [ ] syntax highlighting for code blocks
   * Waiting for scymtym to release his syntax highlighting library.
5. [ ] use different colors for code block metadata

** New fold state - docstring + argument list                      :feature:
:PROPERTIES:
:CREATED:  2022-06-18T19:52:21+0530
:CUSTOM_ID: new-fold-state-docstring-+-argument-list
:END:

** New commands - "Jump to generic" and "Show methods"             :feature:
:PROPERTIES:
:CREATED:  2022-06-18T20:21:27+0530
:CUSTOM_ID: new-commands-jump-to-generic-show-methods
:END:
"Jump to generic" accepts a method and jumps to the definition of its generic function.

"Show methods" is a shorthand for the [[#filter-tree-command][filter tree command]] - it can -
1. accept a generic function and filter the tree to the methods implementing it,
2. accept a class and filter the tree to the methods which specialize on the class and/or its subclasses and/or its superclasses.

(How do these translate to other languages?)

** Display reader macros                                           :feature:
:PROPERTIES:
:CUSTOM_ID: display-reader-macros
:END:
#+BEGIN_QUOTE
<contrapunctus> Is it possible for client code to temporarily modify Eclector to treat reader macros differently? I want to detect and display them as they appear in a source file...so perhaps I could have Eclector just return them as strings, for instance?

<scymtym> that is possible, but i have the general impression that you are underestimating the difficulty of what you are trying to do

<beach> I think scymtym would be the person to answer that.  Reader macros are invoked as soon as the relevant macro character is seen.  The macro can then do any number of things to the stream.  So I guess you would have to remember every character read.

<scymtym>  specifically, the client can define a method on ECLECTOR.READER:CALL-READER-MACRO

<scymtym>  but without calling the actual reader macro function, there is generally no way to know which characters should be consumed and thus when to continue "normal" reading

<beach>  You could make a Gray stream that remembers the characters being read during the invocation of the reader macro.
#+END_QUOTE

** WYSIWYG editing                                                 :feature:
:PROPERTIES:
:CUSTOM_ID: wysiwyg-editing
:END:

** Support for other languages                                     :feature:
:PROPERTIES:
:CUSTOM_ID: support-other-languages
:END:
e.g. Scheme, APL, Forth

** Support for other formats                                       :feature:
:PROPERTIES:
:CREATED:  2022-06-14T08:50:08+0530
:CUSTOM_ID: support-other-formats
:END:
1. Org literate programs
2. [[https://github.com/melisgl/mgl-pax][MGL-PAX]]

* Experimental
:PROPERTIES:
:CREATED:  2022-06-05T22:07:06+0530
:CUSTOM_ID: experimental
:END:

** Sort by topology/dependency
:PROPERTIES:
:CREATED:  2022-06-19T19:45:59+0530
:CUSTOM_ID: sort-by-topology-dependency
:END:
Most programs have an inherent topological order - definitions build on other definitions, modules build on other modules. But when a reader browses a conventional codebase in a file manager (or on a forge such as GitHub), they are confronted with an alphabetically-sorted listing of directories and files that is devoid of this order.

Topology could be determined by parsing build description files (e.g. system definitions for Common Lisp), among others.

** Flexible formatting of code
:PROPERTIES:
:CREATED:  2022-06-07T12:03:18+0530
:CUSTOM_ID: flexible-formatting-code
:END:
Rather than displaying the source code as formatted in the text files -
1. use an easily-customizable pretty-printer to display the CST, and
2. insert source comments in the output

This makes code indentation conventions entirely unnecessary. Each user can view the code as they prefer, and even adapt the presentation to their needs in the moment.

** View code as graph
:PROPERTIES:
:CREATED:  2022-06-07T12:04:05+0530
:CUSTOM_ID: view-code-as-graph
:END:
moon-child's suggestion. https://github.com/org-roam/org-roam-ui comes to mind.

Interaction could look like this -
1. The mouse wheel changes the global "magnification" (projects -> directories -> files -> code). This is analogous to =org-global-cycle=.
2. Clicking on a node zooms in/out for that node (in the same "projects -> directories -> files -> code" order). This is analogous to =org-cycle=.
3. Clicking on a definition node opens an editor window for it.

* Local variables                                                  :noexport:
:PROPERTIES:
:CUSTOM_ID: local-variables
:END:
# Local Variables:
# eval: (add-hook 'org-insert-heading-hook (lambda nil (save-excursion (org-set-property "CREATED" (format-time-string "%FT%T%z")))) nil t)
# End:
