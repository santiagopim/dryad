(in-package :cl)
(defpackage :dryad.clim
  (:use :clim :clim-lisp)
  (:import-from :alexandria #:circular-list)
  (:import-from :uiop #:read-file-string)
  (:import-from :concrete-syntax-tree #:source)
  (:import-from :trivia #:match)
  (:import-from :cl-colors2 #:as-rgb #:red #:green #:blue)
  (:import-from :dryad
                #:extract #:indent
                #:load-path #:load-path-1
                #:make-tree #:make-tree-1
                #:make-source-file
                #:code-block-name
                ;; classes and slots
                #:node #:name #:location #:fold-state
                #:indentation #:depth #:children #:language
                #:source-file #:extension
                #:code-block #:language)
  (:export #:run-dryad #:display))
(in-package :dryad.clim)

(defun make-rgb-color-from-hex (hex)
  (with-slots (red green blue) (as-rgb hex)
    (apply #'make-rgb-color
           (mapcar #'float (list red green blue)))))

(define-application-frame dryad ()
  ((trees :initarg :trees
          :initform nil
          :accessor trees
          :documentation
          "List of `node' instances currently displayed by the UI.")
   (heading-size-by-depth
    :initarg :heading-size-by-depth
    :initform t
    :accessor heading-size-by-depth
    :documentation
    "Non-nil means enable scaling of heading sizes based on their depth.")
   (heading-colors :initarg :heading-colors
                   :initform (circular-list
                              (make-rgb-color-from-hex "fb2874")
                              (make-rgb-color-from-hex "fd971f")
                              (make-rgb-color-from-hex "9c91e4")
                              (make-rgb-color-from-hex "5ca8dd")
                              (make-rgb-color-from-hex "fb5d96")
                              (make-rgb-color-from-hex "92c4e8"))
                   :accessor heading-colors
                   :documentation
                   "A circular list of colors to use for headings.")
   (code-block-text-style :initarg :code-block-text-style
                          :accessor code-block-text-style
                          :initform (make-text-style :fix :roman nil)
                          :documentation
                          "Text style for display of source code blocks."))
  (:menu-bar t)
  (:pointer-documentation t)
  (:panes (dryad :application
                 :height (graft-height (find-graft))
                 :width (graft-width (find-graft))
                 :display-function 'display-pane
                 :incremental-redisplay t
                 :background (make-rgb-color-from-hex "232629")
                 :foreground (make-rgb-color-from-hex "eff0f1"))
          (int :interactor
               :width (/ (graft-width (find-graft)) 2)
               :background (make-rgb-color-from-hex "232629")
               :foreground (make-rgb-color-from-hex "eff0f1")))
  (:layouts (default (horizontally () dryad int))))

(defun display-pane (frame stream)
  (with-output-recording-options (stream :record t :draw nil)
    (with-slots (trees) frame
      (if trees
          (loop for tree in trees
                do (display tree (fold-state tree) stream))
          (format stream "Nothing to display. Add something!"))))
  (stream-replay stream +everywhere+))

(defgeneric display (object fold-state stream &optional arg)
  (:documentation "Print OBJECT to STREAM."))

;; Methods to display node headings. I probably wrote them this way to
;; make it easy for users to replace them for individual node types.
(defun display-heading-default (object class fold-state stream)
  (unless (eq fold-state :hidden)
    (with-slots (name indentation depth) object
      (with-output-as-presentation (stream object class)
        (with-drawing-options (stream :ink (elt (heading-colors *application-frame*) depth))
          (format stream "~%~A" (indent name indentation depth))
          (match fold-state
            ((or :folded :outline)
             (format stream "..."))))))))

(defmethod display :before
    ((file dryad:source-file) fold-state stream &optional arg)
  (declare (ignore arg))
  (display-heading-default file 'dryad:source-file fold-state stream))

(defmethod display :before
    ((dir dryad:directory) fold-state stream &optional arg)
  (declare (ignore arg))
  (display-heading-default dir 'dryad:directory fold-state stream))

(defmethod display :before
    ((code-block dryad:code-block) fold-state stream &optional arg)
  (declare (ignore arg))
  (display-heading-default code-block 'dryad:code-block fold-state stream))

(defmethod display :after
    ((block dryad:code-block) fold-state stream &optional arg)
  "Display a newline after the contents of BLOCK, when its FOLD-STATE
is not :HIDDEN, :OUTLINE, or :FOLDED."
  (declare (ignore node arg))
  (match fold-state
    ((not (or :hidden :outline :folded))
     (format stream "~%"))))

(defmethod display
    ((node dryad:node) fold-state stream &optional arg)
  (declare (ignore node fold-state stream arg)))

(defun filter-by-type (type list)
  (remove-if-not (lambda (obj)
                   (typep obj type))
                 list))

(defmethod display
    ((dir dryad:directory) (fold-state t) stream &optional arg)
  (declare (ignore arg))
  (match fold-state
    ((or :outline :content)
     (with-output-as-presentation (stream dir 'dryad:directory)
       (with-slots (children) dir
         (let ((files   (filter-by-type 'source-file children))
               (subdirs (filter-by-type 'dryad:directory children)))
           (loop for file in files
                 do (with-slots (fold-state) file
                      (display file fold-state stream)))
           (loop for subdir in subdirs
                 do (with-slots (fold-state) subdir
                      (display subdir fold-state stream)))))))))

(defmethod display
    ((file source-file) fold-state stream &optional arg)
  "Display FILE and the definitions within it."
  (declare (ignore arg))
  (match fold-state
    ((or :outline :content)
     (with-slots (location children name fold-state indentation depth) file
       (with-output-as-presentation (stream file 'source-file)
         (loop for form in children
               do (with-slots (fold-state) form
                    (display form fold-state stream (location file)))))))))

(defmethod display
    ((code-block code-block) (fold-state (eql :outline)) stream &optional file)
  "No-op to handle display of OUTLINE'd code-blocks."
  (declare (ignore code-block fold-state stream file)))

(defmethod display
    ((code-block code-block) (fold-state (eql :content)) stream &optional file)
  "Display text from FILE using the source locations provided by CODE-BLOCK.
Text is extracted from FILE to retain and display source comments within the
code."
  (with-slots (name indentation depth fold-state) code-block
    (let* ((source    (source code-block))
           (substring (extract (first source) (rest source) file)))
      (with-drawing-options (stream :text-style (code-block-text-style *application-frame*))
        (with-output-as-presentation (stream code-block 'code-block)
          (format stream "~%~a" (indent substring indentation depth)))))))

(defun update-child-fold-states (node new-state)
  "Update fold state of NODE's children to NEW-STATE, recursively."
  (let ((children (children node)))
    (when children
      (loop for obj in children
            do (setf (fold-state obj) new-state)
               (update-child-fold-states obj new-state)))
    node))

(defgeneric cycle-fold-state (node)
  (:documentation "Return NODE with fold-state updated."))

(defmethod cycle-fold-state ((node node))
  "Cycle fold state for directories and files between :FOLDED,
:OUTLINE, and :CONTENT."
  (case (fold-state node)
    (:folded
     (setf (fold-state node) :outline)
     (update-child-fold-states node :folded))
    (:outline
     (setf (fold-state node) :content)
     (update-child-fold-states node :content))
    (:content
     (setf (fold-state node) :folded)
     (update-child-fold-states node :hidden))))

(defmethod cycle-fold-state ((code-block code-block))
  "Switch fold-state of CODE-BLOCK between :FOLDED and :CONTENT."
  (setf (fold-state code-block)
        (if (eq (fold-state code-block) :folded)
            :content
            :folded)))

(define-dryad-command (com-refresh :name t) ()
  (with-slots (trees) *application-frame*
    (setf trees
          (loop for tree in trees
                collect (make-tree (location tree))))))

(define-dryad-command (com-insert-tree :name t :menu t) ((pathname 'pathname))
  (load-path pathname)
  (pushnew (make-tree pathname) (trees *application-frame*)))

(define-dryad-command (com-remove-tree :name t :menu t) ((node 'node))
  (with-slots (trees) *application-frame*
    (setf trees (remove node trees :test #'equal))))

(define-dryad-command (com-cycle :name t :menu t) ((node 'node))
  (cycle-fold-state node)
  (format (find-pane-named *application-frame* 'int)
          "node: ~S ~S" node (fold-state node)))

(define-presentation-to-command-translator com-cycle
    (node com-cycle dryad)
    (node)
  (list node))

(defun run-dryad (&rest pathnames)
  (let ((trees (loop for path in pathnames
                     do (load-path path)
                     collect (make-tree path))))
    (bt:make-thread
     (lambda ()
       (run-frame-top-level
        (make-application-frame 'dryad :trees trees))))))

;; 1. try to load project
;;    * accept either a directory or a system definition
;; 2. read files
;; 3. display trees
