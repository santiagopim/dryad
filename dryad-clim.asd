(defsystem     dryad-clim
  :version     "0.0.1"
  :serial      t
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :description "A CLIM code browser to visualize and edit any program as a tree"
  :depends-on  (:mcclim :cl-colors2 :bordeaux-threads :trivia
                        :dryad :dryad-cl)
  :components  ((:file "clim")))
