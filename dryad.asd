(defsystem     "dryad"
  :version     "0.0.1"
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :description "Backend library for the Dryad code browser"
  :depends-on  (:concrete-syntax-tree :cl-fad :alexandria :cl-ppcre)
  :serial      t
  :components  ((:module "core"
                 :serial t
                 :components ((:file "package")
                              (:file "utils")
                              (:file "settings")
                              (:file "language")
                              (:file "tree")
                              (:file "default-view")))))
