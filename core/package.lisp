(in-package :cl)
(defpackage :dryad
  (:use :cl)
  (:shadow #:directory)
  (:import-from :cl-ppcre
                #:create-scanner
                #:regex-replace-all)
  (:import-from :uiop
                #:directory-files
                #:subdirectories
                #:enough-pathname
                #:directory-pathname-p
                #:pathname-parent-directory-pathname)
  (:import-from :cl-fad #:pathname-as-file)
  (:import-from :alexandria
                #:when-let*
                #:assoc-value)
  (:export
   #:*settings* #:register-language
   #:language
   #:load-path #:load-path-1
   #:compute-children
   #:make-tree #:make-tree-1
   #:make-source-file
   #:code-block-name
   ;; classes and slots
   #:node #:name #:location #:fold-state #:indentation #:depth #:children
   #:directory #:make-directory
   #:source-file #:extension
   #:code-block #:language
   #:view
   #:default-view
   #:applicable-views
   #:applicable-view-p
   ;; utils
   #:pathname-extension-keyword
   #:extract
   #:indent
   #:string-to-keyword))
