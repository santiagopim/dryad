(in-package :dryad)

(defclass default-view (view)
  ((filter-predicates
    :initarg :filter-predicates
    :initform '(uiop:hidden-pathname-p)
    :accessor filter-predicates
    :documentation
    "List of predicates to filter files and directories. If any
    predicate returns non-nil, the file/directory is not collected."))
  (:default-initargs
   :name "Default"
   :description
   "Recursvely display all subdirectories, supported files, and code found in a directory. Subdirectories and files may be filtered with any Lisp predicates.")
  (:documentation
   "Default generic view for files and directories."))

(register-view (make-instance 'default-view))

(defun keep-pathname? (pathname view)
  "Return nil if PATHNAME matches any of VIEW's filter-predicates.
VIEW must be an instance of `default-view'."
  (with-slots (filter-predicates) view
    (loop for pred in filter-predicates
          never (funcall pred (pathname-as-file pathname)))))

(defmethod compute-children
    ((code-block code-block) (view t) ext &optional (depth 0))
  "No-op for instances of `dryad:code-block', since they have no children."
  nil)

#+(or)
(defmethod compute-children
    ((file source-file) (view t) ext &optional (depth 0))
  (with-slots (location extension depth) file
    ( location
      (and extension (string-to-keyword extension))
      depth)))

(defmethod compute-children
    ((directory dryad:directory) (view default-view) ext &optional (depth 0))
  (with-slots (location depth) directory
    (let* ((depth   (1+ depth))
           (files   (loop for file in (directory-files location)
                          when (and (keep-pathname? file view)
                                    (make-source-file file :depth depth))
                            collect it))
           (subdirs (loop for subdir in (subdirectories location)
                          when (keep-pathname? subdir view)
                            collect (make-directory subdir :depth depth))))
      (append files subdirs))))
