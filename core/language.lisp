(in-package :dryad)

(defclass language ()
  ((name :initarg :name
         :initform (error "Name not supplied"))))

(defmethod print-object ((language language) stream)
  (print-unreadable-object (language stream :type t)
    (with-slots (name) language
      (format stream "~A" name))))

(defun register-language (language keyword)
  "Insert LANGUAGE into `languages', with KEYWORD as the key.
If `languages' already contains an association with KEYWORD, update
the association."
  (with-slots (languages) *settings*
    (let* ((existing (assoc keyword languages)))
      (if existing
          (setf (rest (assoc keyword languages))
                language)
          (push (cons keyword language)
                languages))
      languages)))

(defun get-language (keyword)
  (with-slots (languages) *settings*
    (assoc-value languages keyword)))

(defgeneric code-block-name (code-block)
  (:documentation "Return a name for CODE-BLOCK as a string. Frontends
  display this name in the code block heading."))

(defgeneric code-block-metadata (code-block)
  (:documentation "Return metadata for CODE-BLOCK as a string. Frontends
  display this metadata in the code block heading."))
