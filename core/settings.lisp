(in-package :dryad)

(defclass settings ()
  ((languages :initarg :languages
              :initform nil
              :accessor languages
              :documentation
              "Association list of languages supported by Dryad.")
   (views :initarg :views
          :initform nil
          :accessor views
          :documentation
          "List of `view' instances available to Dryad.")))

(defvar *settings* (make-instance 'settings))
